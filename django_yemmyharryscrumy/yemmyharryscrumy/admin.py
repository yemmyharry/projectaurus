from django.contrib import admin
from .models import ScrumyHistory, GoalStatus, ScrumyGoals
from django.contrib.auth.models import User

# Register your models here.
admin.site.register(ScrumyHistory)
admin.site.register(GoalStatus)
admin.site.register(ScrumyGoals)