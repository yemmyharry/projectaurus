from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.db import models
from . import models
from .models import ScrumyHistory, GoalStatus, ScrumyGoals, User
from django.contrib.auth.models import Group
import random
from django.contrib.auth import login
from django.contrib.auth import authenticate
from . import forms
developer_group = Group.objects.get(name = 'Developer')
group_owner = Group.objects.get(name = 'Owner')
group_qa = Group.objects.get(name = 'Quality Assurance')
group_admin = Group.objects.get(name = 'Admin')



# def index(request):
#     return HttpResponse(ScrumyGoals.objects.filter(goal_name="Learn Django"))


# this index enables successful signup
def index(request): 
    if request.method == 'POST': 
         form = forms.SignupForm(request.POST) 
         if form.is_valid():
            new_user = form.save(commit = False)
            # new_user.set_password(request.POST.get('password'))
            new_user.set_password(new_user.password)
            new_user.save()
            developer_group.user_set.add(new_user)
            return HttpResponse('Your account has been created successfully')   
    else: 
         form = forms.SignupForm()
         context = {
             'form': form,
         }
         return render(request, 'yemmyharryscrumy/index.html', context)




# def move_goal(request,goal_id):
#     try: 
#          goal = ScrumyGoals.objects.get(goal_id=goal_id)
#     except Exception as e:
#         context = {'error': 'A record with that goal id does not exist',}
#         return render(request, 'yemmyharryscrumy/exception.html', context)
#     else: return HttpResponse(goal.goal_name)
#     return HttpResponse(goal.goal_name)


# this enables movement of users in different groups and set boundaries to what is allowable
   
def move_goal(request, goal_id):
    if request.method == 'POST':
        goal = ScrumyGoals.objects.get(goal_id = goal_id)
        verify_goal = GoalStatus.objects.get(status_name = "Verify Goal")
        done_goal = GoalStatus.objects.get(status_name = "Done Goal")
        if request.user.groups.filter(name__in=[developer_group]).exists() and goal.created_by != request.user.username or request.user.groups.filter(name__in=[group_owner]).exists() and goal.created_by != request.user.username:
            return render(request, 'yemmyharryscrumy/exception.html', {'error': 'Permission denied'})
        elif request.user.groups.filter(name__in=[developer_group]).exists() and goal.goal_status == done_goal:
            return render(request, 'yemmyharryscrumy/exception.html', {'error': 'Permission denied'})
        elif request.user.groups.filter(name__in=[developer_group]).exists() and request.POST.get('group1') == 'Done Goal':
            return render(request, 'yemmyharryscrumy/exception.html', {'error': 'Permission denied'})
        elif request.user.groups.filter(name__in=[group_qa]).exists() and goal.goal_status != verify_goal and goal.created_by != request.user.username and (request.POST.get('group1') in ['Done Goal', 'Daily Goal', 'Weekly Goal', 'Verify Goal']):
            return render(request, 'yemmyharryscrumy/exception.html', {'error': 'Permission denied'})
        elif request.user.groups.filter(name__in=[group_qa]).exists() and goal.goal_status == verify_goal and goal.created_by != request.user.username and (request.POST.get('group1') in ['Daily Goal', 'Weekly Goal', 'Verify Goal']):
            return render(request, 'yemmyharryscrumy/exception.html', {'error': 'Permission denied'})
        else:  
            goal.goal_status = GoalStatus.objects.get(status_name = request.POST.get('group1'))
            goal.save()
            return redirect("yemmyharryscrumy:home")                      
    else:
        return render(request, 'yemmyharryscrumy/movegoal.html')


def add_goal(request):
    if request.method == 'POST':
        form = forms.CreateGoalForm(request.POST)
        if form.is_valid():
            user = form.cleaned_data['user']
            new_user = form.save(commit= False)




        list = [] #created an empty list/array

        goal_list = ScrumyGoals.objects.all() 

        for i in goal_list:     #go through all instances of scrumygoals
            list.append(int(i.goal_id))     #add the number in list to the goalid of scrumygoals

        while True:
            number = random.randint(1000, 9999)  #generates a random number

            if number in list:
                continue
            else:
            
             
                new_user.goal_id=number
                new_user.created_by=user
                new_user.moved_by=user
                new_user.owner=user
                new_user.goal_status= GoalStatus.objects.get(status_name="Weekly Goal")
                new_user.save()
              
                return redirect("yemmyharryscrumy:home")

    else:
        form = forms.CreateGoalForm()
        context = {
            'form': form,
        }
    return render( request, 'yemmyharryscrumy/addgoal.html', context)
    
def home(request):
    context = { 'users': User.objects.all(),
                 'weekly_goals': ScrumyGoals.objects.filter(goal_status=GoalStatus.objects.get(status_name="Weekly Goal")),
                 'daily_goals': ScrumyGoals.objects.filter(goal_status=GoalStatus.objects.get(status_name="Daily Goal")),
                 'verify_goals': ScrumyGoals.objects.filter(goal_status=GoalStatus.objects.get(status_name="Verify Goal")),
                 'done_goals': ScrumyGoals.objects.filter(goal_status= GoalStatus.objects.get(status_name="Done Goal")),
                    }
    return render(request, 'yemmyharryscrumy/home.html', context)
    
