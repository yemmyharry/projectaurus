from django.db import models
from django.contrib import admin
from django.contrib.auth.models import User

# Create your models here.

class GoalStatus(models.Model):
    status_name     =       models.CharField(max_length=200)

    def __str__(self):
        return self.status_name

class ScrumyGoals(models.Model):
    goal_name   =   models.CharField(max_length=200)
    goal_id     =   models.IntegerField(default=0)
    created_by  =   models.CharField(max_length=200)
    moved_by    =   models.CharField(max_length=200)
    owner       =   models.CharField(max_length=200)
    goal_status =   models.ForeignKey(GoalStatus, on_delete=models.PROTECT, related_name='number_of_goals')
    user        =   models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_name')


    class Meta:
        ordering  =  ['-id']

    def __str__(self):
        return self.goal_name



class ScrumyHistory(models.Model):
    moved_by         =   models.CharField(max_length=200)
    created_by       =   models.CharField(max_length=200)
    moved_from       =   models.CharField(max_length=200)
    moved_to         =   models.CharField(max_length=200)
    time_of_action   =   models.DateTimeField(auto_now_add=True)
    goal             =   models.ForeignKey(ScrumyGoals, on_delete=models.CASCADE, related_name='score')


    def __str__(self):
        return self.created_by