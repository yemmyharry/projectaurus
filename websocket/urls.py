from django.urls import path, include
from . import views

app_name='websocket'

urlpatterns = [
    path('test/', views.test, name='test'),
    path('disconnect/', views.disconnect, name='disconnect'),
    path('connect/', views.connect, name='connect'),
    path('get_recent_messages/', views.get_recent_messages, name='getRecentMessages'),
    path('send_message/', views.send_message, name='sendMessage'),
]